const mongoose = require('mongoose');

const tagSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    dishes : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Dish' }]
});

tagSchema.virtual('tags', {
    ref: 'Dish', //The Model to use
    localField: '_id', //Find in Model, where localField
    foreignField: 'tag', // is equal to foreignField
});

module.exports = mongoose.model('Tag', tagSchema);