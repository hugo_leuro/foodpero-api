const mongoose = require('mongoose');

const dishSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    tags: [{type: mongoose.Schema.Types.ObjectId, ref: 'Tag', required: true}],
    dishImages: [{type: String, required: true}],
    time: Number,
});

dishSchema.virtual('dishes', {
    ref: 'Tag', //The Model to use
    localField: '_id', //Find in Model, where localField
    foreignField: 'dish', // is equal to foreignField
});

// Set Object and Json property to true. Default is set to false
dishSchema.set('toObject', {virtuals: true});
dishSchema.set('toJSON', {virtuals: true});


module.exports = mongoose.model('Dish', dishSchema);