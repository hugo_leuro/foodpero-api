const express = require('express');
const router = express.Router();
const multer = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        console.log(file);
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    fileFilter: fileFilter
});

const DishesController = require('../controllers/dish')

//Dish
router.get('/', DishesController.dishes_get_all)
router.get('/:dishId', DishesController.dishes_get_one)
router.post('/', upload.array('dishImages', (3)), DishesController.dishes_create_dish)
router.delete('/:dishId')

module.exports = router;