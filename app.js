const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const dishRoutes = require('./api/routes/dish');
const tagRoutes = require('./api/routes/tag');

mongoose.connect('mongodb+srv://admin:' + process.env.NODEMON_PASSWORD + '@foodpero.7lazb.mongodb.net/myFirstDatabase?retryWrites=true&w=majority')
app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type,Accept, Authorization');

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, GET, DELETE')
        return res.status(200).json({})
    }
    next();
}))

// Routes
app.use('/dishes', dishRoutes);
app.use('/tags', tagRoutes);

module.exports = app;