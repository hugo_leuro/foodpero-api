const Dish = require("../models/dish");
const Tag = require("../models/tag");
const mongoose = require("mongoose");

exports.dishes_create_dish = async (req, res) => {
    let images = [];
    if (req.files) {
        req.files.forEach(file => {
            images.push(file.path);
        })
    }
    const dish = new Dish({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        tags: req.body.tags,
        dishImages: images,
        time: req.body.time,
    });

    await dish.save().then(async result => {
        for (const tagId of dish.tags) {
            const tag = await Tag.findById({_id: tagId})
            tag.dishes.push(dish);
            await tag.save();
        }
    }).catch(error => {
        res.status(500).json({error: error})
    });

    res.status(201).json({
        message: 'Dish has added successful',
        createdDish: dish,
    })
}

exports.dishes_get_all = (req, res) => {
    Dish.find().populate({path: 'tags', select: 'name'}).exec().then(docs => {
        res.status(200).json(docs);
    }).catch(error => {
        res.status(500).json({error: error})
    })
}

exports.dishes_get_one = (req, res) => {
    Dish.findById(req.params.dishId).exec().then(doc => {
        if (doc) res.status(200).json(doc);
        else res.status(404).json({message: 'This dish doesn\'t exist'})
    }).catch(error => {
        res.status(500).json({error: error})
    })
}

exports.dishes_delete_one = (req, res) => {
    Dish.remove({_id: req.params.dishId}).exec().then(result => {
        res.status(200).json(result);
    }).catch(error => {
        res.status(500).json({error: error})
    })
}