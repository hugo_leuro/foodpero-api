const express = require('express');
const router = express.Router();

const TagsController = require('../controllers/tag')

//Dish
router.get('/', TagsController.tags_get_all)
router.get('/:tagId', TagsController.tags_get_one)
router.post('/', TagsController.tags_create_tag)
router.delete('/:tagId', TagsController.tags_delete_one)

module.exports = router;