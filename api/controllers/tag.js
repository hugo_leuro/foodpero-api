const Tag = require("../models/tag");
const mongoose = require("mongoose");
const Dish = require("../models/dish");

exports.tags_create_tag = (req, res) => {
    const tag = new Tag({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
    });

    tag.save().then(result => {
        console.log(result);
    }).catch(error => {
        console.log(error);
    });

    res.status(201).json({
        message: 'Tag has added successful',
        createdTag: tag,
    })
}

exports.tags_get_one = (req, res) => {
    Tag.findById(req.params.tagId).populate({path: 'dishes', select: 'name dishImages time'}).exec().then(doc => {
        if (doc) res.status(200).json(doc);
        else res.status(404).json({message: 'This tag doesn\'t exist'})
    }).catch(error => {
        res.status(500).json({error: error})
    })
}

exports.tags_get_all = (req, res) => {
    Tag.find().populate({path: 'dishes', select: 'name'}).exec().then(docs => {
        console.log(docs)
        res.status(200).json(docs);
    }).catch(error => {
        console.log(error);
        res.status(500).json({error: error})
    })
}

exports.tags_delete_one = (req, res) => {
    Tag.remove({_id: req.params.tagId}).exec().then(result => {
        res.status(200).json(result);
    }).catch(error => {
        res.status(500).json({error: error})
    })
}